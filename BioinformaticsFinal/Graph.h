#include "GraphNode.h"

class Graph
{
public:
	vector<GraphNode> GN; // Vector of nodes of graph
	int nodeCt;    // Size of G 
	string name;

	Graph(int size, string na)
	{
		//set up sizes
		GN.resize(size); //# of rows

		nodeCt = size;

		for (int i = 0; i < nodeCt; i++)
		{
			GN[i].name = CHARMOD + i;
			GN[i].nodeID = i;
			GN[i].height = 0;
		}
		name = na;
	}  // initialize vector of graphNodes

	string toString(string msg);
	void genGraph(char * filename);
}; // Graph 

