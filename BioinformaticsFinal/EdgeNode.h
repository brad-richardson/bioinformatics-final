#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

const unsigned char CHARMOD = 0;

class EdgeNode
{
public:
	int from;
	int to;
	int distance;

	EdgeNode(int from, int to, int dist){
		this->from = from;
		this->to = to;
		this->distance = dist;
	}
	string toString(){
		stringstream out;
		//unsigned char f = CHARMOD + from;
		//unsigned char t = CHARMOD + to;
		//out << " " << f << "=>" << t << " " << setw(2) << distance;
		out << " " << from << "=>" << to << " " << setw(2) << distance;
		out << " ";
		return out.str();
	}
}; // EdgeNode 