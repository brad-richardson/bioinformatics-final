#include <iostream>
#include "Graph.h"
#include <cmath>
#include <fstream>
#include <sstream>

using namespace std;

const string FILENAME = "ebov-med-results.txt";//"ebolavirus-med-results.txt";
const string FILENAME2 = "ebolavirus-med-results.txt";
const string NAMESFILENAME = "ebovNamesFile.txt";//"ebolaVirusNamesFile.txt"
const string NAMESFILENAME2 = "ebolaVirusNamesFile.txt";

vector<vector<int>> getDistanceMatrix() {
	std::vector<std::vector<int>> ebolaVirusDistanceMatrix;

	std::ifstream ebolaVirusStream(FILENAME2);

	int ebolaVirusSize;
	ebolaVirusStream >> ebolaVirusSize;

	ebolaVirusDistanceMatrix.resize(ebolaVirusSize);
	for (int i = 0; i < ebolaVirusDistanceMatrix.size(); ++i) {
		ebolaVirusDistanceMatrix[i].resize(ebolaVirusSize);
	}

	for (int i = 0; i < ebolaVirusDistanceMatrix.size(); ++i) {
		for (int j = 0; j < ebolaVirusDistanceMatrix[i].size(); ++j) {
			ebolaVirusStream >> ebolaVirusDistanceMatrix[i][j];
		}
	}

	return ebolaVirusDistanceMatrix;
}

vector<string> getNodeNames() {
	std::vector<std::string> ebolaVirusNames;
	// Read in names of different sequences
	std::ifstream ebolaVirusNamesFileStream(NAMESFILENAME2);
	while (!ebolaVirusNamesFileStream.eof()) {
		std::string line;
		std::getline(ebolaVirusNamesFileStream, line);
		ebolaVirusNames.push_back(line);
	}
	return ebolaVirusNames;
}

void findClosest(vector<vector<int>> matrix, int &l, int &m){
	int shortestDist = matrix[0][1]; // random value
	l = 0;
	m = 1;
	for (int i = 0; i < matrix.size(); i++){
		for (int j = 0; j < matrix.size(); j++){
			if (i != j && matrix[i][j] < shortestDist){
				shortestDist = matrix[i][j];
				l = i;
				m = j;
			}
		}
	}
}

int pairwiseDist(Graph T, vector<vector<int>> matrix, vector<unsigned char> clustersChar, int i, int j){ // i and j are the indexes of the matrix
	int sum = 0;
	for (int l = 0; l < T.GN[clustersChar[i] - CHARMOD].indexes.size(); l++){
		for (int m = 0; m < T.GN[clustersChar[j] - CHARMOD].indexes.size(); m++)
			sum += matrix[T.GN[clustersChar[i] - CHARMOD].indexes[l]][T.GN[clustersChar[j] - CHARMOD].indexes[m]];
	}
	return sum / (T.GN[clustersChar[i] - CHARMOD].indexes.size() * T.GN[clustersChar[j] - CHARMOD].indexes.size());
}

int findIndexOfLetter(vector<unsigned char> clustersChar, unsigned char letter){
	for (int i = 0; i < clustersChar.size(); i++){
		if (clustersChar[i] == letter)
			return i;
	}
}

Graph UPGMA(vector<vector<int>> matrix, vector<std::string> names, int size){
	Graph T = Graph(matrix.size(), "Tree");
	for (int i = 0; i < size; i++){
		T.GN[i].indexes.push_back(i);
		T.GN[i].setName(names[i]);
	}
	int count = 0;
	vector<unsigned char> clustersChar;
	vector<vector<int>> newMatrix = matrix;
	// set up ClustersChar
	for (int i = 0; i < size; i++)
		clustersChar.push_back(CHARMOD + i);

	while (clustersChar.size() > 1){
		int row, col;
		// find the two closest clusters c1 and c2
		findClosest(newMatrix, row, col);

		unsigned char first = clustersChar[row];
		unsigned char second = clustersChar[col];

		// update clustersChar
		clustersChar.push_back(CHARMOD + size + count);

		// merge c1 and c2 into a new cluster c with |c1| + |c2| elements
		T.GN.push_back(GraphNode(T.GN.size(), CHARMOD + size + count));
		for (int i = 0; i < T.GN[first - CHARMOD].indexes.size(); i++)
			T.GN[T.GN.size() - 1].indexes.push_back(T.GN[first - CHARMOD].indexes[i]);
		for (int i = 0; i < T.GN[second - CHARMOD].indexes.size(); i++)
			T.GN[T.GN.size() - 1].indexes.push_back(T.GN[second - CHARMOD].indexes[i]);

		// add new row and new col for new cluster
		newMatrix.resize(newMatrix.size() + 1); // # of rows
		for (int i = 0; i < newMatrix.size(); ++i)
			newMatrix[i].resize(newMatrix.size()); // # of cols

		for (int i = 0; i < clustersChar.size(); i++){
			if (T.GN[clustersChar[i] - CHARMOD].name != first && T.GN[clustersChar[i] - CHARMOD].name != second && T.GN[clustersChar[i] - CHARMOD].name != T.GN[T.GN.size() - 1].name){
				newMatrix[newMatrix.size() - 1][i] = pairwiseDist(T, matrix, clustersChar, newMatrix.size() - 1, i);
				newMatrix[i][newMatrix.size() - 1] = pairwiseDist(T, matrix, clustersChar, newMatrix.size() - 1, i);
			}
		}

		T.GN[T.GN.size() - 1].height = (newMatrix[row][col]) / 2;
		T.GN[T.GN.size() - 1].adj.push_back(EdgeNode(T.GN.size() - 1, clustersChar[row] - CHARMOD, T.GN[T.GN.size() - 1].height - T.GN[clustersChar[row] - CHARMOD].height));
		T.GN[T.GN.size() - 1].adj.push_back(EdgeNode(T.GN.size() - 1, clustersChar[col] - CHARMOD, T.GN[T.GN.size() - 1].height - T.GN[clustersChar[col] - CHARMOD].height));

		// remove rows and cols from matrix corresponding to c1 and c2
		newMatrix.erase(newMatrix.begin() + row); // (matrix.begin() + #);   # = index of row
		for (int l = 0; l < newMatrix.size(); l++)
			newMatrix[l].erase(newMatrix[l].begin() + row); // # = index of col*/

		// remove char from vector of char clustersChar
		clustersChar.erase(clustersChar.begin() + row);

		int index = findIndexOfLetter(clustersChar, second);

		newMatrix.erase(newMatrix.begin() + index); // (matrix.begin() + #);   # = index of row
		for (int l = 0; l < newMatrix.size(); l++)
			newMatrix[l].erase(newMatrix[l].begin() + index); // # = index of col*/

		// remove char from vector of char clustersChar
		clustersChar.erase(clustersChar.begin() + index);

		count++;
	}
	return T;
}

std::string traverseGraph(Graph &T, int nodeId) {
	std::stringstream ss;
	//Base case
	if (T.GN[nodeId].visited == true) {
		return "";
	}
	T.GN[nodeId].visited = true;
	
	std::vector<int> childNodes;
	std::vector<int> childDistances;
	for (int i = 0; i < T.GN[nodeId].adj.size(); ++i) {
		int childNodeId;
		//Check to see if it is "from" or "to" node
		if (T.GN[nodeId].adj[i].from == nodeId) {
			childNodeId = T.GN[nodeId].adj[i].to;
		}
		else {
			childNodeId = T.GN[nodeId].adj[i].from;
		}

		if (T.GN[childNodeId].visited == false) {
			childNodes.push_back(childNodeId);
			childDistances.push_back(T.GN[nodeId].adj[i].distance);
		}
	}
	// Is leaf node
	// print LeafName
	if (childNodes.size() == 0) {
		ss << T.GN[nodeId].realName;
		//return traverseGraph(T, nodeId + 1);
	}
	else {
		// print child nodes
		ss << "(";
		for (int i = 0; i < childNodes.size(); ++i) {
			ss << traverseGraph(T, childNodes[i]);
			ss << ":" << childDistances[i];
			if (i < childNodes.size() - 1) {
				ss << ",";
			}
		}
		ss << ")";
		ss << T.GN[nodeId].realName;
		// if is first node
	}
	return ss.str();
}

int main(){
	int size = 5;
	//allocate 2D vector
	vector<vector<int>> matrix;
	/*vector<char> GC;

	// set up GC
	for (int i = 0; i < size; i++)
	GC.push_back(CHARMOD + i);
	cout << "GC = ";
	for (int i = 0; i < size; i++){
	cout << GC[i];
	if (i != size - 1)
	cout << ",";
	}
	cout << endl;*/

	//set up sizes
	//matrix.resize(size); //# of rows
	//for (int i = 0; i < size; ++i)
	//	matrix[i].resize(size); //# of cols

	//cout << "Size = " << matrix.size() << endl;

	/*matrix[0] = { 0, 8, 6, 8, 5 };
	matrix[1] = { 8, 0, 10, 2, 5 };
	matrix[2] = { 6, 10, 0, 10, 7 };
	matrix[3] = { 8, 2, 10, 0, 5 };
	matrix[4] = { 5, 5, 7, 5, 0 };*/

	/*matrix[0] = { 0, 2, 4, 4};
	matrix[1] = { 2, 0, 4, 4};
	matrix[2] = { 4, 4, 0, 2};
	matrix[3] = { 4, 4, 2, 0};*/

	/*matrix[0] = { 0, 4, 10, 9 };
	matrix[1] = { 4, 0, 8, 7 };
	matrix[2] = { 10, 8, 0, 9 };
	matrix[3] = { 9, 7, 9, 0 };*/

	matrix = getDistanceMatrix();

	// cout matrix
	/*for (int i(0); i < size; i++){
		for (int j(0); j < size; j++){
			cout << matrix[i][j];
			if (j != size - 1)
				cout << ",";
		}
		cout << endl;
	}*/

	std::vector<std::string> nodeNames = getNodeNames();

	Graph T = UPGMA(matrix, nodeNames, matrix.size());

	ofstream fout("results.txt");
	std::vector<EdgeNode> edgeNodes;
	// print out all edges
	int maxEdgesNode;
	int maxAdjCt = -1;
	for (int i = 0; i < T.GN.size(); i++){
		for (int j = 0; j < T.GN[i].adj.size(); j++){
			//cout << T.GN[i].adj[j].toString();
			//fout << T.GN[i].adj[j].toString();
			
			//edgeNodes.push_back(T.GN[i].adj[j]);
		}
		if (maxAdjCt < T.GN[i].adj.size()) {
			maxAdjCt = T.GN[i].adj.size();
			maxEdgesNode = i;
		}
	}
	//cout << endl;

	/*while (edgeNodes.size() > 0) {
		for (int i = 0; i < edgeNodes.size(); ++i) {

		}
	}*/

	fout << traverseGraph(T, T.GN[T.GN.size()-1].nodeID);
	fout << ";";
	fout.close();
	
	return 0;
}