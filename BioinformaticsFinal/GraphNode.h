#include "EdgeNode.h"

class GraphNode
{
public:
	int nodeID;       // ID of node, from 0 to nodeCt-1
	unsigned char name;	 // Node name from A to Z
	vector<EdgeNode> adj;// adjacency list
	vector<int> indexes;
	bool visited;  // true if visited already
	int height;

	std::string realName;

	GraphNode(int  nodeID = -1, unsigned char name = ' ')
	{
		this->nodeID = nodeID;
		this->name = name;
		visited = false;
	}

	void setName(std::string name) {
		realName = name;
	}
}; // GraphNode 